function reconhecerNumeros(n){
    // verificar números pares
    for(let i =1; i <= n; i++){
      if (i % 2 === 0){
        console.log(`${i} é um número par.`);
      } else {
          continue;
      }
    }
    // verificar números ímpares
    for(let i =1; i <= n; i++){
      if (i % 2 != 0){
        console.log(`${i} é um número ímpar.`);
      } else {
          continue;
      }
    }
    // verificar multiplos de 3
    for(let i =1; i <= n; i++){
      if (i % 3 === 0){
        console.log(`${i} é um número multiplo 3.`);
      } else {
          continue;
      }
    }
    // verificar multiplo de 5
    for(let i =1; i <= n; i++){
      if (i % 5 === 0){
        console.log(`${i} é um número multiplo 5.`);
      } else {
          continue;
      }
    }
    // verificar números primos
    for(let numero = 2; numero <= n; numero++){ 
      let ehPrimo = true;
        for(let divisor = 2; divisor < numero; divisor ++){
          if(numero % divisor === 0){
            ehPrimo = false;
            break;
          }  
        }
          if(ehPrimo === true){
            console.log(`${numero} é um número primo.`);
          } else {
            continue;
          }
      }      
  }
  console.log(reconhecerNumeros(15));